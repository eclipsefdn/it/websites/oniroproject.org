---
title: "Oniro"
date: 2021-10-06T10:00:00-04:00
footer_class: "footer-darker"
header_wrapper_class: "featured-jumbotron-home"
headline: "" 
custom_jumbotron: |
  <h1>
    The open source <strong>Operating System</strong> that provides you with
    a platform to <strong>Think Global and Code Local</strong>
  </h1>
jumbotron_class: "jumbotron-circle"
custom_jumbotron_class: "col-xs-24"
hide_breadcrumb: true
hide_page_title: true
container: "container-fluid"
hide_sidebar: true
show_featured_footer: false
seo_title: "Oniro"
---

{{< home/about-us >}}

{{< home/architecture >}}

{{< home/working-group >}}

{{< home/join-us >}}

{{< home/supporters >}}

{{< home/testimonials >}}
