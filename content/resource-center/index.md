---
title: "Resource Center"
date: 2021-10-06T10:00:00-04:00
footer_class: "footer-darker"
hide_sidebar: true
keywords: ["Resources", "Oniro repositories", "Charter", "Compliance Toolchain", "OpenHarmony"]
layout: "single"
---

The process to create the Oniro Working Group and Top Level Project has been an
intensive collaborative effort by the founding members of the Oniro Working
Group. A number of the resulting resources can be accessed here, currently
hosted at Eclipse:

- [Eclipse Oniro For OpenHarmony](https://github.com/eclipse-oniro4openharmony)
  &mdash; This project hosts the innovations, enhancements and add-ons Oniro is
  developing on top of the OpenHarmony project.
- [Eclipse Oniro Mirrors](https://github.com/eclipse-oniro-mirrors) &mdash;
  This project contains the complete OpenHarmony repositories

Learn more about the Oniro project at the Eclipse Foundation:

- [Eclipse Oniro Top Level Charter](https://projects.eclipse.org/projects/oniro/charter)
- [Eclipse Oniro Top Level Project](https://projects.eclipse.org/projects/oniro)
- [Eclipse Oniro For OpenHarmony](https://projects.eclipse.org/projects/oniro.oniro4openharmony)
- [Eclipse Oniro Compliance Toolchain](https://projects.eclipse.org/projects/oniro.oniro-compliancetoolchain)

For a comprehensive overview of the technical aspects of our project, please
visit our [technical documentation](https://docs.oniroproject.org/) page.
