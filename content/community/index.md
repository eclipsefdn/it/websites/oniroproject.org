---
title: "Community"
date: 2021-10-06T10:00:00-04:00
tags: ["news", "events", "social media", "mailing list", "contact", "chat", "community"]
footer_class: "footer-darker"
hide_sidebar: true
hide_page_title: true
layout: "single"
container: "container-fluid"
---

{{< grid/div class="container padding-top-30 padding-bottom-60" >}}
# Community

Here are several ways you can engage and connect with our community:

* [Public developers mailing list](https://accounts.eclipse.org/mailing-list/oniro-dev)
* [Public working group's mailing list](https://accounts.eclipse.org/mailing-list/oniro-wg)
* [Community's chat](https://docs.oniroproject.org/en/latest/community-chat-platform.html)
* [Follow Oniro on Twitter](https://twitter.com/@oniro_project)
* [Contact Oniro's staff](https://oniroproject.org/join-us/)
{{</ grid/div >}}

{{< grid/section-container class="featured-news padding-y-40 bg-white">}}
  {{< grid/div class="row" isMarkdown="false" >}}
    {{< grid/div class="col-sm-12" isMarkdown="false" >}}
      {{< newsroom/news
          title="News"
          id="news-list-container"
          publishTarget="oniro"
          count="5"
          class="col-sm-24"
          templateId="custom-news-template" templatePath="/js/templates/news-home.mustache"
          paginate="true" >}}
    {{</ grid/div >}}

    {{< grid/div class="col-sm-12 featured-events text-center" isMarkdown="false" >}}
      {{< newsroom/events
          title="Upcoming Events"
          publishTarget="oniro"
          class="col-sm-24"
          containerClass="event-timeline"
          upcoming="1"
          templateId="custom-events-template" templatePath="js/templates/event-list-format.mustache"
          count="4"  includeList="true" >}}
    {{</ grid/div >}}
   {{</ grid/div >}}
{{</ grid/section-container >}}
